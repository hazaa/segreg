# segreg

Segregation.


## Install/Config

* marble [git](https://github.com/scities/marble), python 2 only, must be converted to python 3.
    >git clone https://github.com/scities/marble.git
    >2to3 -w marble

* [pysal](http://pysal.org), Python Spatial Analysis Library
   *   install: 
       *    sudo apt-get install libspatialindex-dev
       *    python3 -m pip install pysal 
   *   config: export PYSALDATA=$HOME/local/data/pysal_data
   *   get datasets: sac = load_example('Sacramento1') ; or fetcha_all()
                       

* [pysal segregation](https://github.com/pysal/segregation)
   *   https://github.com/pysal/segregation/blob/master/notebooks/inference_wrappers_example.ipynb
   *   install: 
       *    python3 -m pip install segregation descartes 

## Examples
