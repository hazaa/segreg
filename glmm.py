"""
http://eshinjolly.com/pymer4/

LMM+py: https://towardsdatascience.com/a-bayesian-approach-to-linear-mixed-models-lmm-in-r-python-b2f1378c3ac8

==== STATS
but du modèle: 
 Yij = mu + Aj + Uij  ??
 quelles corrélations entre les classes "white"/...., en mettant de côté la part due au site.

 ?? x.vec~0+spp+(1|site)+(0+spp|site)  ??

rappel:
 l'effet aléatoire est de moyenne nulle. sa variance est inconnue. modalités infinies.
 dans notre cas, c'est le site.
 
 
observations:
 * le résidu (studentisé?) est globalement très faible. Pq ?
   par modalité ? global ?
   qqmath marche pas  ??
 * effets fixes: bonne signif, on retrouve bien la classe "white"/...
 * la queue est longue d'un côté.
 * fixed effect:
   glmer ne renvoie par le F de Fisher ???
   anova(fit.glmm) ?
   on s'attend à un F, qui correspond au rejet de l'hyp H0 (=les moyennes sont les memes, pour toute valeurs de l'effet fixe "race")
   E
 * random effect:   
   la variance de white est faible. La var de hisp et black sont bcp + importantes. Pq  ??
   corr: formule ? quelle interprétation ? quelle significativité ? 
   
 TODO: comparer à modèle sans random effect (pooled). quid de AIC, likeli ??  
 
=== MAXCAL
 y-a-t-il randomness pour chaque site ? faire un plot des traj, site par site. 
"""
