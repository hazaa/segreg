# -*- coding: utf-8 -*-
"""

"""
import numpy as np
from scipy.stats import multinomial,chi2

from segreg.chi2 import *

import pandas as pd
import geopandas as gpd
#import segregation
import libpysal
from libpysal.examples import get_path,load_example,explain
#from segregation.aspatial import Entropy,Dissim, MultiDissim
#from segregation.inference import SingleValueTest
import matplotlib.pyplot as plt

# ------------------------
# Synthetic datasets

def test_true_distribution():
    """
    samples taken from the 'true' distribution. 
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.chi2.html?highlight=chi%20square
    """ 
    k=200 ; p = np.random.rand(k); p=p/np.sum(p) ; n=100
    #p = np.array([0.3, 0.2, 0.5] ); k = p.shape[0] ; n=1000  

    rv = multinomial(n, p)
    nsamp = 1000
    samp = rv.rvs(nsamp)
    D2_l=[]
    accept_l=[]
    alpha = 0.05
    for i in range(nsamp):
        D2 = D2_statistic(n,p,samp[i,:])
        D2_l.append( D2 )
        accept,thr = chi2_test(D2, k, alpha=alpha)
        accept_l.append( accept)
    # false rejection rate
    #assert np.isclose( np.sum(1*accept_l)/nsamp ,     1-alpha)
    print( 'empirical rejection rate: {} ;  alpha:{}'.format(1-np.sum(1*accept_l)/nsamp ,     alpha) )
    # plot
    df = k-1
    x = np.linspace(chi2.ppf(0.01, df), chi2.ppf(0.99, df), 100)
    fig, ax = plt.subplots(1, 1)
    y = chi2.pdf(x, df)
    ax.plot(x, y, 'r-', lw=5, alpha=0.6, label='chi2 pdf')
    ax.plot( [thr,thr], [np.min(y),np.max(y)], 'r--' )
    ax.hist(D2_l, density=True, histtype='stepfilled', alpha=0.2)
    ax.legend(loc='best', frameon=False)
    plt.show()

def test_false_distribution():
    """
    samples not taken from the 'true' distribution. 
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.chi2.html?highlight=chi%20square
    """ 
    p     = np.array([0.3, 0.2, 0.5] ); k = p.shape[0] ; n=1000  
    p_hyp = np.array([0.3, 0.25, 0.45] );
    rv = multinomial(n, p)
    nsamp = 1000
    samp = rv.rvs(nsamp)
    D2_l=[]
    accept_l=[]
    alpha = 0.05
    for i in range(nsamp):
        D2 = D2_statistic(n, p_hyp ,samp[i,:])
        D2_l.append( D2 )
        accept,thr = chi2_test(D2, k, alpha=alpha)
        accept_l.append( accept)
    # false rejection rate
    #assert np.isclose( np.sum(1*accept_l)/nsamp ,     1-alpha)
    print( 'empirical rejection rate: {} ;  alpha:{}'.format(1-np.sum(1*accept_l)/nsamp ,     alpha) )
    # plot
    df = k-1
    x = np.linspace(chi2.ppf(0.01, df), chi2.ppf(0.99, df), 100)
    fig, ax = plt.subplots(1, 1)
    y = chi2.pdf(x, df)
    ax.plot(x, y, 'r-', lw=5, alpha=0.6, label='chi2 pdf')
    ax.plot( [thr,thr], [np.min(y),np.max(y)], 'r--' )
    ax.hist(D2_l, density=True, histtype='stepfilled', alpha=0.2)
    ax.legend(loc='best', frameon=False)
    plt.show()

# the same with more groups
    
        
# ------------------------    
# Real datasets    
    
def test_sac():
    """ """   
    path = get_path("sacramentot2.shp")
    s_map = gpd.read_file(path)
    gdf = s_map[['geometry', 'HISP_', 'TOT_POP']]
    n = 
    p =
    samp = 
    k = 
