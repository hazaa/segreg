# -*- coding: utf-8 -*-

"""
REFS:

[Pardo 06] Pardo, "Statistical Inference Based on Divergence Measures", Taylor & Francis, 2006
"""
import numpy as np
from scipy.stats import multinomial, shapiro, anderson, probplot
import matplotlib.pyplot as plt

# ---------------
# 
def stat_multinomial_pardo(h_theta,h_theta_hat,si,pi,n):
    """
    test statistic [Pardo 06] Thm 2.3 for a multinomial population

    $\sigma_\phi(\mathbf{p})^2 = \sum_{i=1}^M s_i^2 p_i - (\sum_{i=1}^M s_i p_i)^2  $
    $s_i=\phi'(p_i)$.

    parameter:
    ----------
    si: array, shape=(ngroup,)
    $s_i=\phi'(p_i)$.
    
    pi: array, shape=(ngroup,)

    output:
    -------
    Z: float
    the value of the statistic.
    """
    sigma_sq = np.sum( (si**2)* pi) - (np.sum( si* pi))**2 
    sigma = np.sqrt(sigma_sq)
    return np.sqrt(n)*(h_theta_hat-h_theta)/sigma



def test_stat_multinomial_pardo_smallsample():
    """smallsample n=1 """
    pass

"""TEST: role of pop, n_group, nsamp/nset...
         power of the test, ROC curve ?   
    """ 
 
    
def test_pysal_pardo():
    """
    dataset:  geo; few groups/many groups
    compare tests:  pardo, barthelemy, rey         
    """
    pass

def test_pysal_pardo_time():
    """
    dataset: Block-level Earnings in NYC (2002-14)
            https://geodacenter.github.io/data-and-lab//LEHD_Data/
    compute: entropy time=t1, t2
            single value
            two value (t1-t2)
            statistic
    compare to pardo         
    """
    pass
