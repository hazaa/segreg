#######################################################################
# Fit a LMM and a GLMM (generalized linear mixed model) borrowed from ecology
# with human population data at city scale
# using R package lme4. 
# 
# Code below adapted from [Warton 15] see startercode.R
#
# MODEL
# 
# REFS:
#    [Warton et al. 15] "So Many Variables: Joint Modeling in Community Ecology"
#    Trends in Ecology & Evolution, December 2015
#    http://dx.doi.org/10.1016/j.tree.2015.09.007
#    code: startercode.R
#    (accessed from: https://www-sciencedirect-com.ezproxy.u-pec.fr/science/article/pii/S0169534715002402#sec0050 )
#
#    [Hastie et al. 2017] "An Introduction to Statistical Learning" James, Witten, Hastie, Tibshirani
#
#    lme4/doc/lmer.pdf , https://cran.r-project.org/web/packages/lme4/index.html
#    
#    notebooks: https://ms.mcmaster.ca/~bolker/R/misc/foxchapter/bolker_chap.html
#               https://stats.idre.ucla.edu/r/dae/mixed-effects-logistic-regression/ 
#               https://slcladal.github.io/mixedregressions.html#3_mixed-effects_binomial_logistic_regression
#    course: http://www.bristol.ac.uk/cmm/learning/course.html
#
# REMINDER about formulas in R:
# * lme4.pdf, function lmer: "Random-effects terms are distinguished by vertical bars (|)"
# * R formula help:  "To avoid this confusion, the function ‘I()’ can be used to bracket
#       those portions of a model formula where the operators are used in their arithmetic sense"
# * R-intro (https://cran.r-project.org/doc/manuals/r-release/R-intro.html#Formulae-for-statistical-models)
#      " M_1 : M_2   The tensor product of M_1 and M_2. If both terms are factors, then the “subclasses” factor. "
# 
# REMINDER about interaction terms:
# 
#  cf [Hastie et al. 2017] 3.3.2 Extensions of the Linear Model, see eq.(3.35)
#                  and  3.6.4 Interaction Terms (R examples)
#
# DEPENDENCES
#   sudo  apt-get install libgsl-dev 
#   R packages:  mvabund,   lme4, corrplot, foreign


# TODO
# * standard R notebook pour urban 
# * tests segreg : Louf,  pysal, Pardo

library(foreign)   # for read.dbf
library(lme4)
library(lattice)
library("gridExtra")     ## for grid.arrange()
library(corrplot)

path = "/home/aurelien/.local/lib/python3.5/site-packages/libpysal/examples/Phoenix_ACS/phx/phx.dbf"
x = read.dbf(path)

x.vec = c(x$white_rt*x$pop/100, x$black_rt*x$pop/100, x$hisp_rt*x$pop/100)
n.site = dim(x)[1]
n.spp = 3

X = data.frame(income = rep(scale(x$inc), n.spp), spp = rep(c('white','black','hisp'), each=n.site), site = rep(dimnames(x)[[1]], n.spp) )

## -------------
## LINEAR MODEL
m0.lmer = lmer(x.vec~0+spp+(1|site)+(0+spp|site), data=X,control = lmerControl(check.nobs.vs.nRE = "ignore")) 
# uniq un intercept, pas de résidu par spp
#m0.lmer = lmer(x.vec~0+spp+(1|site), data=X,control = lmerControl(check.nobs.vs.nRE = "ignore")) 
# PAS d'INTERCEPT?? :
#m0.lmer = lmer(x.vec~0+spp+(0+spp|site), data=X,control = lmerControl(check.nobs.vs.nRE = "ignore")) 

# summary
print(summary(m0.lmer),correlation=FALSE) # To look at estimated parameter values

plot(m0.lmer)

# residuals grouped by treatment
p1 <- plot(m0.lmer,id=0.05,idLabels=~.obs) 
p2 <- plot(m0.lmer,ylim=c(-1.5,1),type=c("p","smooth"))
grid.arrange(p1,p2,nrow=1)


# fixed effects
anova(m0.lmer)

# variance decomposition
as.data.frame(VarCorr(m0.lmer))

str(rr1 <- ranef(m0.lmer))
dotplot.ranef.mer(rr1,condVar=TRUE)
## -----------------
## GENERALIZED MODEL (poisson)
## fit the GLMM using lme4 and look at results
#fit.glmm_income = glmer(x.vec~0+spp+spp:income+(1|site)+(0+spp|site), data=X, family=poisson())
m0.glm = glm(x.vec~0+spp, data=X, family=poisson())
m0.glmer = glmer(x.vec~0+spp+(0+spp|site), data=X, family=poisson())
#m0.glmer = glmer(x.vec~0+spp+(1|site)+(0+spp|site), data=X, family=poisson())

aic.glm <- AIC(logLik(m0.glm))
aic.glmer <- AIC(logLik(m0.glmer))
aic.glm; aic.glmer;
# AIC of glmer smaller => including the random intercepts is justified.

# summary
print(summary(m0.glmer),correlation=FALSE) # To look at estimated parameter values
confint(m0.glmer, method="Wald") # 95% confidence intervals for model parameters. Very approximate though, other more computationally intensive options are available.

# variance decomposition
as.data.frame(VarCorr(m0.glmer))

fitted(m0.glmer)

# residuals grouped by treatment
# NB: very small std for all. 
p1 <- plot(fit.glmm,id=0.05,idLabels=~.obs)
p2 <- plot(fit.glmm,ylim=c(-1.5,1),type=c("p","smooth"))
grid.arrange(p1,p2,nrow=1)


op <- par(mfcol = c(2,2))
qqnorm(rr1$site$sppwhite)
qqline(rr1$site$sppwhite)
qqnorm(rr1$site$sppblack)
qqline(rr1$site$sppblack)
qqnorm(rr1$site$spphisp)
qqline(rr1$site$spphisp)
#grid.arrange(p1,p2,p3,nrow=1)


# check random effect
str(rr1 <- ranef(fit.glmm))
pdf('ranef_phx.pdf')
dotplot.ranef.mer(rr1,condVar=TRUE)  ## default
dev.off()
#qqmath(ranef(fit.glmm,condVar=TRUE))
#lattice::dotplot
#lattice::qqmath


## Use corrplot package to calculate and plot residual correlations between species, e.g. possibly due to species interaction etc...
vrcorrs=VarCorr(fit.glmm)
corrs=attr(vrcorrs$site.1,"corr")
pdf('cor_ranef_phx.pdf')
corrplot(corrs, diag = F, type = "lower", title = "", method = "color", tl.srt = 45)
dev.off()
# Q: test: ces niveaux de cor sont-ils significatifs ???


