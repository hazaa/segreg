# -*- coding: utf-8 -*-

"""
REFS:

PySAL 
    lipbysal: https://pysal.org/libpysal/
              fetch datasets: https://pysal.org/libpysal/notebooks/examples.html
    segregation module:    https://knaaptime.com/papers/#segregation_module
    

tests:
    random labeling [Sastre-Gutierrez et al. 2013] Sastre-Gutierrez, M. and Rey, S. J. (2013). "Space-time income distribution dynamics in Mexico" Annals of GIS, 19(3):195–207
    random spatial permutations [Anselin 1995] "Local indicators of spatial association-LISA", Geographical Analysis, 27(2):93–115

decompose spatial/attribute:
    [Rey et al.19] "Comparative Spatial Segregation Analytics", https://knaaptime.com/papers/pdfs/comparative.pdf?pdf=compatative_seg
"""


import pandas as pd
import geopandas as gpd
#import segregation
import libpysal
from libpysal.examples import get_path,load_example,explain
from segregation.aspatial import Entropy,Dissim, MultiDissim
from segregation.inference import SingleValueTest
import matplotlib.pyplot as plt

      

# -----------------------------------------------
# PYSAL+segregation

# ---
# Helper

def load_sacramento():
    """ 
    TODO: https://github.com/pysal/libpysal/blob/46220d8fa77b3cc1a2a6ab02a881ba9abaf90411/notebooks/Example%20Datasets.ipynb
    
    #fetch_all()
    sac = load_example('Sacramento1')
        
    OR
    load from https://geodacenter.github.io/data-and-lab/
    path="/home/aurelien/.local/lib/python3.5/site-packages/libpysal/examples/sacramento"
    """
    path = get_path("sacramentot2.shp")
    if path is None: raise ValueError('empty path')
    s_map = gpd.read_file(path)
    #s_map.columns
    gdf = s_map[['geometry', 'HISP_', 'TOT_POP']]
    return gdf

def load_nyc_earn():
    """ 
    explain('NYC Earnings')
    nyc = load_example('NYC Earnings')
    #nyc.get_file_list()
    
    https://geodacenter.github.io/data-and-lab//LEHD_Data/
    
    Variable 	Description
    YR 	02 to 14 (2002 to 2014)
    GEOID10 	unique ID (FIPS code)
    C000_YR 	Total number of jobs
    CE01_YR 	Number of jobs with earnings $1250/month or less
    CE02_YR 	Number of jobs with earnings $1251/month to $3333/month
    CE03_YR 	Number of jobs with earnings greater than $3333/month
    ...         ...
    """
    path = get_path("NYC Area2010_2data.shp")
    if path is None: raise ValueError('empty path')
    nyc_map = gpd.read_file(path)
    #nyc_map.plot()
    return nyc_map

def load_phoenix_race():
    """
    https://geodacenter.github.io/data-and-lab//phx/
    
    available()
    load_example('Phoenix ACS')
    phx = load_example('Phoenix ACS')
    
    Var         Descr
    GEOID10 	Census Bureau’s Tract id
    pop 	Population count
    pop_dens 	Population density (including only land area)
    white_rt 	Rate of white population (as a percentage of the total population)
    black_rt 	Rate of black population (as a percentage of the total population)
    hisp_rt 	Rate of hispanic population (as a percentage of the total population)
    ...         ...
    
    """
    path = get_path("phx.shp")
    if path is None: raise ValueError('empty path')
    phx_map = gpd.read_file(path)
    #nyc_map.plot()
    return phx_map
    
    

def basic_stats_nyc_earn():
    """
    
    observations:
    * pdf( nb_job_wholecity_cat_i) \approx exp. Can be fitted by Binomial or Poisson
    * pdf( delta) \approx laplace (as difference of exp)
        https://en.wikipedia.org/wiki/Laplace_distribution#Occurrence_and_applications
        https://en.wikipedia.org/wiki/Variance_gamma_process
    * total number of jobs is NOT MONOTONE for job_cat 01 and 02 (decreasing for y \in [03,09])
    """
    nyc_map = load_nyc_earn()
    # simple pdf
    job_cat = '01'    
    delta = nyc_map['CE01_03']-nyc_map['CE01_02']
    plt.hist(delta,50,histtype='step')
    plt.show()
    bins = 100
    # time series, sum per job_cat
    sums_t=list()
    #years = range(3,9)
    years = range(3,14)
    for i in years: 
        if i<10: name = 'CE{}_0{}'
        else : name = 'CE{}_{}'
        sums = list()
        for job_cat in ['01','02','03']:
            sums.append( np.sum( nyc_map[name.format(job_cat,str(i))]))
        sums_t.append(sums)
    sums_t = np.array(sums_t)    
    for j in range(3): plt.plot(years, sums_t[:,j], label=str(j) )
    plt.legend()
    plt.show()
    # pdf(delta) over time
    # 
    for i in range(3,9): 
        delta = nyc_map['CE{}_0{}'.format(job_cat,str(i))]-nyc_map['CE{}_0{}'.format(job_cat,str(i-1))]
        plt.hist(delta,bins,histtype='step')
    plt.show()    
# ---
# Tests: helper func
def test_load_sacramento():
    """ compute and plot proportion of hispanics"""
    gdf = load_sacramento()
    gdf['composition'] = gdf['HISP_'] / gdf['TOT_POP']
    gdf.plot(column = 'composition',
         cmap = 'OrRd', 
         figsize=(20,10),
         legend = True)
    plt.show()     
    
# ---
# Tests: Measures

def test_pysal_seg_measure_aspatial_single_group():
    """
    aspatial, single group
    https://github.com/pysal/segregation#calculating-segregation-measures
    #https://github.com/pysal/segregation/blob/master/notebooks/aspatial_examples.ipynb  
    """
    gdf = load_sacramento()
    index = Entropy(gdf, 'HISP_', 'TOT_POP')
    index.statistic

    
def test_pysal_seg_measure_aspatial_multi_group():
    """
    aspatial, multigroup    
    """
    nyc_map = load_nyc_earn()
    index = MultiDissim(nyc_map, ['CE01_10', 'CE02_10', 'CE03_10'])
    index.statistic
    # nan !!!!!!!!!!!!!!!!!!!
    
def test_pysal_seg_measure_spatial_single():
    """spatial, single group """
    pass
    
def test_pysal_seg_measure_spatial_multi():
    """spatial, multigroup """
    pass
    
# ---
# Inference
    
def test_pysal_seg_inference_single():
    """
    single value test
    https://github.com/pysal/segregation/blob/master/notebooks/inference_wrappers_example.ipynb
    https://github.com/pysal/segregation#testing-for-statistical-significance
    """    
    index = Entropy(gdf, 'HISP_', 'TOT_POP')
    index.statistic
    s = SingleValueTest(index, iterations_under_null = 1000, null_approach = "evenness", two_tailed = True)
    s.p_value
    s.est_sim.mean()
    s.plot()
    plt.show() 	
    #    
    s = SingleValueTest(index, iterations_under_null = 5000, null_approach = "systematic", two_tailed = True)
    s.p_value
    s.est_sim.mean()
    s.plot()
    plt.show() 

def test_pysal_seg_inference_two():
    """ two-value test"""
    pass

# ---
# Decomposition
def test_pysal_decompose():
    """
    decomposition: spatial/attribute components

    cf [Rey et al.19]
    https://github.com/pysal/segregation/blob/master/notebooks/decomposition_wrapper_example.ipynb
    """
    pass
