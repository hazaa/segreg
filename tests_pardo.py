# -*- coding: utf-8 -*-
from statistic_pardo import stat_multinomial_pardo
from scipy.stats import multinomial

def test_stat_multinomial_pardo():
    """
    population  =multinomial
    phi-entropy =Shannon
    
    Z must be normally distributed, if the null hypothesis is verified.
    
    REFS:
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.multinomial.html?highlight=multinomial#scipy.stats.multinomial
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.anderson.html#scipy.stats.anderson
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.probplot.html
    """
    nsamp_per_set = 100
    n_set = 3000
    # sample from multinomial with param p
    p_target = [0.3, 0.2, 0.5]
    #p_samp=[0.3, 0.2, 0.5]
    p_samp=[0.25, 0.25, 0.5]
    p_samp=[0.28, 0.22, 0.5]
    #p_samp=[0.33, 0.33, 0.33]
    pop=100
    rv = multinomial(pop, p_samp)
    samp = rv.rvs(nsamp_per_set * n_set)
    # h_p (=ground truth)
    h_p = float(multinomial.entropy(pop, p_target))
    # si: $s_i=\phi'(p_i)$, $phi(x)=-x \log(x) $
    si = -(1.+np.log(p_target))
    # compute Z
    Z=np.zeros(n_set)
    for i in range(n_set):
        # 
        subsamp = samp[i*nsamp_per_set:(i+1)*nsamp_per_set,: ]
        # p_hat
        p_hat= np.mean(subsamp,axis=0)/pop
        # h_p_hat
        h_p_hat = float(multinomial.entropy(pop, p_hat))
        # compute Z
        Z[i] = stat_multinomial_pardo(h_p,h_p_hat,si,p,nsamp_per_set)
    # check Z is normal
    probplot(Z,dist='norm',plot=plt)
    plt.show()
    # some tests
    #r=shapiro(Z)
    #print(r)
    r=anderson(Z,dist='norm')
    print(r)
    if np.any(r.statistic<r.critical_values):
        print('H0 not rejected (Anderson)')        
    else:
        print('H0 rejected (Anderson)')    
