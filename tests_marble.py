"""
some tests with the library [Marble]
and the code of article [Louf et al.16] in the repo [Patterns] 

conversion marble python2->python3
* run 2to3
* rename directory to marble3
* in each file (including __init__.py): 'import marble' ->'import marble3'


see marble3/tests/test_representation.py


REFS:
    [Louf et al.16] "Patterns of residential segregation" by R. Louf & M. Barthelemy  DOI:10.1371/journal.pone.0157476
    [Marble] R.Louf https://github.com/scities/marble
    [Patterns] R.Louf https://github.com/scities/patterns-of-segregation
"""

import marble3 as mb
import csv
from math import sqrt
import pandas as pd
import matplotlib.pyplot as plt


HOME = '/home/aurelien/local/git/extern/socio-spatial-stratification/'

def test_marble():
    city = {"A":{0: 10, 1:0, 2:23},
          "B":{0: 0, 1:10, 2:8}}
    co = mb.concentration(city)
    #binomial test
    
"""    
 copied from github.com/scities/socio-spatial-stratification
 socio-spatial-stratification/bin/analysis/representation_categories.py
"""

def success_test(val,var,level=0.95):
    return 1*(val> 1.-2.57*sqrt(var) and val< 1.+2.57*sqrt(var))

def get_msa(dir_home):
    msa = {}
    with open(dir_home+'data/names/msa.csv', 'r') as source:
        reader = csv.reader(source, delimiter='\t')
        reader.__next__()
        for rows in reader:
            msa[rows[0]] = rows[1]
    return msa

def test_overrepresentation(dir_home=HOME):
    """
    for a given list of MSA, compute the rate of success to the representation rate.
    """
    msa = get_msa(dir_home)
    results = {}
    imax=10
    for i,city in enumerate(msa):
        d=overrepresentation_1city_allincomebins(dir_home=HOME,city=city)
        results[city]=d
        if i>imax: break
    df = pd.DataFrame(results).transpose()        
    # plot        
    df.iloc[:,[0,1,2,13,14,15]].plot() 

    plt.xlabel('city (msa)')
    plt.ylabel('success rate, representation test')
    plt.rc('font',size=17)
    plt.show() 
    
def overrepresentation_1city_allincomebins(dir_home=HOME,city=6922):
    """
    1 city only
    all income bins in original data are kept (no grouping)
    """
    ## Import category composition  
    households = {}
    with open(dir_home+'data/income/msa/%s/income.csv'%city, 'r') as source:
        reader = csv.reader(source, delimiter='\t')
        reader.__next__()
        for rows in reader:
            households[rows[0]] = {c:int(h) for c,h in enumerate(rows[1:])}
        ## Compute representation and variance
        rep = mb.representation(households)
    # success or fail representation test
    success = {}
    for bg in rep: 
       success[bg]={} 
       for cat in sorted(rep[bg].keys()): 
          val, var = rep[bg][cat] 
          success[bg][cat]=success_test(val,var)
          
    df= pd.DataFrame(success).transpose() 
    return df.mean().to_dict() 
    
def test_representation_1city_allincomebins():
    """
    many cities
    all income bins in original data are kept (no grouping)
    
    Code about overrepresentation can be reused: see in [Patterns]/bin/plot_inter-urban.py, line 71-.
    Corresponds to Fig.2 in [Louf et al.16]
    """
    pass
