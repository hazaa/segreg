# -*- coding: utf-8 -*-
"""
chi2 test for uniform distribution 


https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.chi2.html?highlight=chi%20square
"""
import numpy as np
from scipy.stats import chi2
import matplotlib.pyplot as plt
#import pandas as pd
#import geopandas as gpd

def D2_statistic(n,p,count):
    """ 
    compute the $D^2$ statistic in the chi-square test
    $D^2 = \sum_{i=1}^k \frac{ (count_i -n p_i)^2 }{n p_i}$
    
    arguments:
    ----------
    n: float
    the total population
    
    p: array, shape=(k,)
    the expected probabilities for each class k
    
    count: array, shape=(k,)
    the count for each class k
    
    output:
    -----------
    D2 : float
    the $D^2$ statistic in the chi-square test
    """
    if n<=0 : raise ValueError('n msut be >0')
    if not isinstance(p,np.ndarray): raise ValueError('p must be np.ndarray')
    if not isinstance(count,np.ndarray): raise ValueError('count must be np.ndarray')
    n_p = n*p
    D2 = np.sum( (count-n_p)**2/n_p )
    return D2
    
def chi2_test(D2, k, alpha=0.05):
    """
    chi-square test
    
    Arguments:
    ----------
    D2 : float
    the $D^2$ statistic in the chi-square test    
    
    k : int
    the number of groups
    
    alpha: float
    the significance level
    
    output:
    -----------
    H0_accept: bool
    
    thr: float
    the threshold of the critical region
    """
    if k<=0 : raise ValueError('k must be >0')
    thr = chi2.ppf(1-alpha, k-1)
    return D2<thr, thr 
